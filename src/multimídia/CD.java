/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimídia;

/**
 *
 * @author rwietter
 */
public class CD {
    private String title;
    private String artist;
    private int numberTracks;
    private int timeDuration;
    private boolean isHave;
    private String description;

    public CD(String title, String artist, int numberTracks, int timeDuration) {
        this.title = title;
        this.artist = artist;
        this.numberTracks = numberTracks;
        this.timeDuration = timeDuration;
        this.isHave = false;
    }

    public void setIsHave(boolean isHave) {
        this.isHave = isHave;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public boolean getIsHave() {
        return isHave;
    }

    public String getDescription() {
        return description;
    }

    public String getArtist() {
        return artist;
    }

    public int getNumberTracks() {
        return numberTracks;
    }

    public int getTimeDuration() {
        return timeDuration;
    }
}
  