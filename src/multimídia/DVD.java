/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimídia;

/**
 *
 * @author rwietter
 */
public class DVD {
    private String title;
    private String director;
    private int timeDuration;
    private boolean isHave;
    private String description;

    public DVD(String title, String director, int timeDuration) {
        this.title = title;
        this.director = director;
        this.timeDuration = timeDuration;
        this.isHave = false;
        this.description = "";
    }

    public void setIsHave(boolean isHave) {
        this.isHave = isHave;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }

    public int getTimeDuration() {
        return timeDuration;
    }

    public boolean getIsHave() {
        return isHave;
    }

    public String getDescription() {
        return description;
    }
}
