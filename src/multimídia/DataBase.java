/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimídia;

import java.util.ArrayList;

/**
 *
 * @author rwietter
 */
public class DataBase {
    private ArrayList<CD> cds;
    private ArrayList<DVD> dvds;

    public DataBase() {
        cds = new ArrayList<CD>();
        dvds = new ArrayList<DVD>();
    }

    public void setCds(CD cd) {
        this.cds.add(cd);
    }

    public void setDvds(DVD dvd) {
        this.dvds.add(dvd);
    }

    public ArrayList<CD> getCds() {
        return cds;
    }

    public ArrayList<DVD> getDvds() {
        return dvds;
    }
    
        
}
