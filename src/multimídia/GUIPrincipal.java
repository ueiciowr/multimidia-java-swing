/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multimídia;

/**
 *
 * @author rwietter
 */
public class GUIPrincipal extends javax.swing.JFrame {
    private DataBase dataBase = new DataBase();
    private GuiaCadastroDvd cadastroDVD = new GuiaCadastroDvd(new java.awt.Frame(), true, dataBase);
    private GUIRelatorioDVD relatorioDVD = new GUIRelatorioDVD(new java.awt.Frame(), true, dataBase);
    private GUICadastroCD cadastroCD = new GUICadastroCD(new java.awt.Frame(), true, dataBase);
    private GUIRelatorioCD relatorioCD = new GUIRelatorioCD(new java.awt.Frame(), true, dataBase);
    private GUITelaSobre telaSobre = new GUITelaSobre(this, true);
    

    public GUIPrincipal() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jM_Cadastro = new javax.swing.JMenu();
        jMI_DVD = new javax.swing.JMenuItem();
        jMI_CD = new javax.swing.JMenuItem();
        jM_Relatorio = new javax.swing.JMenu();
        jMI_Relatorio_DVD = new javax.swing.JMenuItem();
        jMI_Relatorio_CD = new javax.swing.JMenuItem();
        jM_Sobre = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jM_Cadastro.setText("Cadastro");

        jMI_DVD.setText("DVD");
        jMI_DVD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMI_DVDActionPerformed(evt);
            }
        });
        jM_Cadastro.add(jMI_DVD);

        jMI_CD.setText("CD");
        jMI_CD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMI_CDActionPerformed(evt);
            }
        });
        jM_Cadastro.add(jMI_CD);

        jMenuBar1.add(jM_Cadastro);

        jM_Relatorio.setText("Relatório");

        jMI_Relatorio_DVD.setText("DVD");
        jMI_Relatorio_DVD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMI_Relatorio_DVDActionPerformed(evt);
            }
        });
        jM_Relatorio.add(jMI_Relatorio_DVD);

        jMI_Relatorio_CD.setText("CD");
        jMI_Relatorio_CD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMI_Relatorio_CDActionPerformed(evt);
            }
        });
        jM_Relatorio.add(jMI_Relatorio_CD);

        jMenuBar1.add(jM_Relatorio);

        jM_Sobre.setText("Sobre");
        jM_Sobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jM_SobreActionPerformed(evt);
            }
        });

        jMenuItem1.setText("Sobre");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jM_Sobre.add(jMenuItem1);

        jMenuBar1.add(jM_Sobre);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 279, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMI_CDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMI_CDActionPerformed
        this.cadastroCD.setVisible(true);
    }//GEN-LAST:event_jMI_CDActionPerformed

    private void jMI_DVDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMI_DVDActionPerformed
        this.cadastroDVD.setVisible(true);
    }//GEN-LAST:event_jMI_DVDActionPerformed

    private void jMI_Relatorio_DVDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMI_Relatorio_DVDActionPerformed
        this.relatorioDVD.setVisible(true);
    }//GEN-LAST:event_jMI_Relatorio_DVDActionPerformed

    private void jM_SobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jM_SobreActionPerformed
        
    }//GEN-LAST:event_jM_SobreActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        this.telaSobre.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMI_Relatorio_CDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMI_Relatorio_CDActionPerformed
        this.relatorioCD.setVisible(true);
    }//GEN-LAST:event_jMI_Relatorio_CDActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUIPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUIPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUIPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUIPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUIPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem jMI_CD;
    private javax.swing.JMenuItem jMI_DVD;
    private javax.swing.JMenuItem jMI_Relatorio_CD;
    private javax.swing.JMenuItem jMI_Relatorio_DVD;
    private javax.swing.JMenu jM_Cadastro;
    private javax.swing.JMenu jM_Relatorio;
    private javax.swing.JMenu jM_Sobre;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    // End of variables declaration//GEN-END:variables
}
